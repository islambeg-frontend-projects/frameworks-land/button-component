<h1 align="center">Button Component</h1>

<div align="center">
   Solution for a challenge from  <a href="http://devchallenges.io" target="_blank">Devchallenges.io</a>.
</div>

<div align="center">
  <h3>
    <a href="https://islambeg-frontend-projects.gitlab.io/frameworks-land/button-component/">
      Demo
    </a>
    <span> | </span>
    <a href="https://gitlab.com/islambeg-frontend-projects/frameworks-land/button-component">
      Solution
    </a>
    <span> | </span>
    <a href="https://devchallenges.io/challenges/TSqutYM4c5WtluM7QzGp">
      Challenge
    </a>
  </h3>
</div>

## Table of Contents

- [Overview](#overview)
  - [Built With](#built-with)
- [Features](#features)
- [How to use](#how-to-use)
- [Contact](#contact)
- [Acknowledgements](#acknowledgements)

## Overview

I wanted to try making components with Svelte so I decided to give it a go.

While building the components, the main ideas were to make the defaults sensible
and also to leave the space for customizations.

I also used Typescript as a main programming languages even though this is a really
small projected and its use is unlikely to be justifiable. However it still proved
to be a nice experience and helped to catch a few bugs early on.

As this was a learning exercise I didn't bother with making it a full fledged module. After all, do we really need yet another npm package? :)

### Built With

- [Svelte](https://svelte.dev/)
- [Typescript](https://www.typescriptlang.org/)
- [Sass](https://sass-lang.com/)
- [Vite](vitejs.dev/)

## Features

- 4 color options (customizable through CSS variables)
- 3 size options (customizable through CSS variables)
- Disabled state
- Hover and focus effects
- Text and outline variants
- With and without shadow
- With and without icons (out of the box support for Google Material round icons)

## How To Use

To clone and run this application, you'll need [Git](https://git-scm.com) and [Node.js](https://nodejs.org/en/download/) (which comes with [npm](http://npmjs.com)) installed on your computer. From your command line:

```bash
# Clone this repository
$ git clone https://gitlab.com/islambeg-frontend-projects/frameworks-land/button-component

# Install dependencies
$ npm install

# Run dev server
$ npm run dev
```

You may also wish to change the showcase app base path in the `vite.config.ts`.

## Acknowledgements

- [Material Icons by Google](https://material.io/resources/icons/?style=round)

## Contact

- Email islambeg@proton.me
- Gitlab [@islambeg](https://gitlab.com/islambeg)
